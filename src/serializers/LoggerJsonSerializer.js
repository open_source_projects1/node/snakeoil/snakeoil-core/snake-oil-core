import LoggerSerializer from "../LoggerSerializer.js"

class LoggerJsonSerializer extends LoggerSerializer {

    constructor(options) {
        super(options);
    }

    serialize(meta, input) {
        let obj = {...input, ...meta}
        if(this._options.pretty === true){
            return JSON.stringify(obj, null, 4)
        } else {
            return JSON.stringify(obj)
        }

    }
}

export default LoggerJsonSerializer