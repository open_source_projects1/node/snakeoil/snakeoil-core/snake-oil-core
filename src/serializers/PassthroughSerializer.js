import LoggerSerializer from "../LoggerSerializer.js"

class PassthroughSerializer extends LoggerSerializer {

    constructor(options) {
        super(options);
    }

    serialize(meta, input) {
        return {...input, ...meta}
    }
}

export default PassthroughSerializer