import StackTraceParser from "./StackTraceParser.js";

function inputToMessageObject(input){
    return {message: input}
}
export default function preFill(logger){
    logger
        .registerSerializerForType(Error, function (input) {
            return {message: input.toString(), stack: StackTraceParser(input.stack), cause: input}
        })
        .registerSerializerForType("string", inputToMessageObject)
        .registerSerializerForType([undefined, null], function () {
            return {message: ""}
        })
        .registerSerializerForType("object", (input) => {
            return input
        })
        .registerSerializerForType(Array, (input) => {
            return Object.assign({}, input)
        })
        .registerSerializerForType(Date, inputToMessageObject)
        .registerSerializerForType("number", inputToMessageObject)
        .registerSerializerForType("boolean", inputToMessageObject)
        .registerSerializerForType("float", inputToMessageObject)
        .registerSerializerForType("symbol", function (input) {
            return {message: input.toString()}
        })
        .registerSerializerForType("function", (input) => {
            return {message: input.name + "<Function>()"}
        })
        .registerDefaultValueHandler("timestamp", function () {
            return new Date()
        })
        .registerDefaultValueHandler("callee", function () {
            let caller = StackTraceParser(new Error().stack)
            let callee = null;
            for (let i = caller.length - 1; i > 0; i--) {
                let methodName = caller[i].methodName.split(".")
                if (caller[i].file.includes("file://") && methodName.length === 2 && this._levels.indexOf(methodName[1]) !== -1) {
                    if (i + 1 in caller) {
                        callee = caller[i + 1]
                    }
                    break;
                }
            }
            return callee
        })
    return logger
}