import path from "node:path"
import fs from "node:fs"
import WildcardMatcher from "./WildcardMatcher.js";
import Timestring from "./Timestring.js";
/*
    Helping utility class to cache given file names
    and make sure file names will change over time for example on day switches at 00:00
 */
class FileDescriptorProvider {


    constructor({filepath, prefix, name, postfix, ending, delimiter = "", gcOptions = {postfix: "__deleteME"}}) {
        gcOptions = Object.assign({strategy : "rename", prefix: "deleteME__", postfix: "__deleteME", maxAge : "30d"}, gcOptions)
        this._options = {filepath, name, prefix, postfix, ending, delimiter, gcOptions}
        this._cache = {}

        if(!this._options.strategy) this._options.strategy = "plain"
    }

    /**
     * Find old log files and treat them
     */
    runGc(){
        if(typeof this._options.gcOptions !== "object") throw new Error("invalid gcOptions provided")
        if(typeof this._options.gcOptions.maxAge !== "string") throw new Error("invalid gcOptions.maxAge string provided")
        let _path = path.normalize(this._options.filepath).replace(/\\/g, "/")

        let { prefix, postfix, ending, delimiter} = this._options
        if(!prefix) prefix = ""
        if(!postfix) postfix = ""
        if(!ending) ending = ""
        if(!delimiter) delimiter = ""

        let fileName = [prefix, "*", postfix].join(delimiter) + (ending.startsWith(".") ? ending : "."+ ending)
        let filePaths = fs.readdirSync( _path )
        const isMatch = WildcardMatcher(fileName, { separator: delimiter })
        for (const _filePath of filePaths) {
            let _fullPath = path.join(_path, _filePath).replace(/\\/g, "/")
            if(isMatch(path.basename(_filePath))){
                let fileStats = fs.statSync(_fullPath)
                if(fileStats.isFile() && ((new Date() - fileStats.birthtime) / 1000) > Timestring(this._options.gcOptions.maxAge)) {
                    if(this._options.gcOptions.strategy === "rename"){
                        fs.renameSync(_fullPath, path.dirname(_fullPath) + "/" + this._options.gcOptions.prefix + path.basename(_fullPath) + this._options.gcOptions.postfix)
                    } else if(this._options.gcOptions.strategy === "remove"){
                        fs.unlinkSync(_fullPath)
                    }
                }
            }

        }
    }

    /**
     * return file descriptor and file name file path
     * @returns {string}
     */
    _getFileName(){
        let {name, prefix, postfix, ending, delimiter} = this._options
        let _name = ""
        if(!name) _name = ""
        if(!prefix) prefix = ""
        if(!postfix) postfix = ""
        if(!ending) ending = ""
        if(!delimiter) delimiter = ""

        // allow function as name to enable dynamic file naming
        // by using a date format function for example
        // to catch day switches and provide file descriptor to new file
        if(typeof name === "function"){
            _name = name.apply(this)
        }

        return [prefix, _name, postfix].join(delimiter) + (ending.startsWith(".") ? ending : "."+ ending)
    }

    _getFullPath(filename){
        return path.join(this._options.filepath, filename).replace(/\\/g, "/")
    }

    _makeDirIfNotExistsSync(filepath){
        if(!fs.existsSync(path.dirname(filepath))){
            fs.mkdirSync(path.dirname(filepath), { recursive: true })
        }
    }

    // https://nodejs.org/dist/latest-v10.x/docs/api/fs.html#fs_file_system_flags
    provideSync(flags = "a", mode){
        let filename = this._getFileName()
        let filepath = this._getFullPath(filename)
        if(this._cache[filepath]){
            return {fd: this._cache[filepath], filename, filepath}
        }
        this._makeDirIfNotExistsSync(filepath)
        let fd = fs.openSync(filepath, flags, mode);
        this._cache[filepath] = fd
        return {fd: fd, filename, filepath}
    }

    // https://nodejs.org/dist/latest-v10.x/docs/api/fs.html#fs_file_system_flags
    async provideAsync(flags = "a", mode){
        let filename = this._getFileName()
        let filepath = this._getFullPath(filename)
        let _that = this
        return new Promise((resolve, reject) => {
            if(_that._cache[filepath]){
                return resolve({fd: _that._cache[filepath], filename, filepath})
            }
            _that._makeDirIfNotExistsSync(filepath)
            fs.promises
                .open( filepath, flags, mode)
                .then((fd) => {
                    _that._cache[filepath] = fd
                    resolve({fd: fd, filename, filepath})
                })
                .catch(reject)
        })

    }

}

export default FileDescriptorProvider