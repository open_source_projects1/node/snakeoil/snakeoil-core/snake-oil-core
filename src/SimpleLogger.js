import assert from "node:assert"
import LoggerSerializer from "./LoggerSerializer.js"
import LoggerTransport from "./LoggerTransport.js"
import isLambda from "./IsLambda.js"

class SimpleLogger {

    constructor({level}) {
        this._levels = ["debug", "info", "warn", "error"]
        this.levelsMapped = {}
        let counter = 0
        this._transports = {}
        this._errorLevelToTransportMap = {}
        this._serializers = {}
        this._serializersForTypes = []
        this._transformers = {}
        this._meta = {}
        this._defaultValues = {}
        this.level = level

        for (let i = (this._levels.length - 1); i >= 0; i--) {
            this.levelsMapped[this._levels[i]] = counter
            this[this._levels[i]] = this._writeLog(this._levels[i])
            counter++
        }
    }

    _isLambda(func){
        if(isLambda(func)) throw new Error("lambda / arrow functions not supported here")
    }

    isLevel(level){
        return this.level === level
    }

    /**
     * Trigger garbage collection for transports
     * @private
     */
    _triggerGc() {
        for (const transport of this._transports) {

        }
    }

    _runDefaultValueHandlers(){
        let obj = {}
        for (const propertyName of Object.keys(this._defaultValues)) {
            obj[propertyName] = this._defaultValues[propertyName].apply(this)
        }
        return obj
    }

    _triggerSerializerForType(inputValue){
        for (const map of this._serializersForTypes) {
            // for null and undefined checks because typeof null === "object" wtf!!
            if(inputValue === map.type || typeof inputValue === map.type || (map.type != null && (typeof map.type === "object" || typeof map.type === "function") && inputValue instanceof map.type)){
                return map.func(inputValue)
            }
        }

        throw new Error("inputValue ["+ inputValue + "] had no matching type serializer")
    }

    _extractLevels(levels){
        assert.ok(levels !== undefined && levels !== null, "levels should be defined")
        if(typeof levels === "string" && levels !== "*"){
            if(this._levels.indexOf(levels) !== -1) {
                return [levels]
            }
            throw new Error("level: "+ levels +" was not found")
        } else if(typeof levels === "string" && levels === "*"){
            return this._levels
        } else if(levels instanceof Array){
            let _levels = []
            for (const level of levels) {
                if(typeof level === "string" && level in this._levels){
                    _levels.push(level)
                }
            }
            return _levels
        } else if(levels instanceof RegExp){
            let _levels = []
            for (const level of this._levels) {
                if(level in this._levels){
                    if(level.exec(levels) !== null){
                        _levels.push(level)
                    }

                }
            }
            return _levels

        }

        if(levels instanceof Array) levels = "["+ levels.join(",") +"]"
        if(levels instanceof RegExp) levels = levels.toString()
        throw new Error("given level pattern "+ levels +" did not match any defined log levels")
    }

    registerDefaultValueHandler(propertyName, func){
        // lambda should not be accepted here because it can lead to not wanted behaviors
        // which will confuse developers
        this._isLambda(func)
        assert.ok(typeof propertyName === "string", "propertyName must be string")
        assert.ok(typeof func === "function", "func must be function")

        this._defaultValues[propertyName] = func
        return this
    }

    registerSerializerForType(type, func){
        //assert.ok(typeof type === "string" || type instanceof Array, "type must be string")
        assert.ok(typeof func === "function", "func must be function")
        if(!(type instanceof Array)) type = [type]
        for (const _type of type) {
            this._serializersForTypes.push({type: _type, func: func})
        }
        return this
    }

    registerSerializer(transportName, serializer){
        assert.ok(serializer instanceof LoggerSerializer, "must be implementation LoggerSerializer class")
        this._serializers[transportName] = serializer
        return this
    }

    registerTransport(transportName, levels, transport) {
        assert.ok(typeof transportName === "string", "transportName must be string")
        assert.ok(typeof levels === "string" || levels instanceof Array, "levels must be string or array")
        assert.ok(transport instanceof LoggerTransport, "must be implementation LoggerTransport class")
        this._transports[transportName] = transport

        let _levels = this._extractLevels(levels)
        for (const level of _levels) {
            if (!this._errorLevelToTransportMap[level]) {
                this._errorLevelToTransportMap[level] = []
            }
            this._errorLevelToTransportMap[level].push({name: transportName, transport: transport})
        }
        return this
    }

    registerTransformer(levels, excludeTarget, func){
        assert.ok(typeof func === "function", "func should be a function")
        let _levels = this._extractLevels(levels)
        for (const level of _levels) {
            if(!this._transformers[level]) this._transformers[level] = []
            this._transformers[level].push({func, exclude: excludeTarget})
        }
        return this
    }

    _writeLogForTransport(_that, transport, level, args){
        if(!this._serializers[transport['name']]){
            throw new Error("no serializer was found for transport with name: '"+ transport['name'] + "'")
        }
        let serializer = this._serializers[transport['name']]
        let metaValues = {..._that._runDefaultValueHandlers(), ..._that._meta, ...{level: level}}

        args = Object.assign({}, _that._triggerSerializerForType(args))

        // before things go to the serializer
        // we apply some transformations when needed
        if(_that._transformers[level] && _that._transformers[level].length > 0){
            for (const transformer of _that._transformers[level]) {
                if(transformer.exclude.indexOf(transport.name) === -1){
                    metaValues = transformer.func.apply(transformer, [metaValues, "meta"])
                    args = transformer.func.apply(transformer, [args, "args"])
                }
            }
        }

        transport["transport"].write(serializer.serialize.apply(serializer, [metaValues, args]))
    }

    _writeLog(level) {
        if(this._levels.indexOf(level) === -1) throw new Error("given error level with name ["+level+"] was not found")
        let _that = this
        return function (args) {
            let errorLevelInput = _that._levels.indexOf(level)
            let errorLevelMax = _that._levels.indexOf(_that.level)
            if(errorLevelMax > errorLevelInput) return;

            if(!_that._errorLevelToTransportMap.hasOwnProperty(level)) console.warn("the following level do not have a transport defined: "+ level)

            if(Object.keys(_that._errorLevelToTransportMap).length === 0){
                throw new Error("there were no transports given")
            }

            if(_that._errorLevelToTransportMap[level]){
                for (const transport of _that._errorLevelToTransportMap[level]) {
                    _that._writeLogForTransport(_that, transport, level, args)
                }
            }

        }
    }
}




export default SimpleLogger