export default function isLambda(func){
    if(typeof func !== "function") throw new Error("invalid function was provided")
    let firstLine =  func.toString()
        .split("\n")['0']
        .replace(/\n|\r|\s/ig, "")

    return (!firstLine.includes("function") && (firstLine.includes(")=>{") || firstLine.includes("){")))
}