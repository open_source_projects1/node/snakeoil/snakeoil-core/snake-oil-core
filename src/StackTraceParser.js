const UNKNOWN_FUNCTION = '<unknown>';

const nodeRe = /^\s*at (?:((?:\[object object\])?[^\\/]+(?: \[as \S+\])?) )?\(?(.*?):(\d+)(?::(\d+))?\)?\s*$/i;

function parseNode(line) {
    const parts = nodeRe.exec(line);

    if (!parts) {
        return null;
    }

    return {
        file: parts[2] +":"+parts[3]+":"+parts[4],
        methodName: parts[1] || UNKNOWN_FUNCTION,
        arguments: [],
        lineNumber: +parts[3],
        column: parts[4] ? +parts[4] : null,
    };
}

export default function parse(stackString) {
    const lines = stackString.split('\n');

    return lines.reduce((stack, line) => {
        const parseResult = parseNode(line)
        if (parseResult) {
            stack.push(parseResult);
        }

        return stack;
    }, []);
}
