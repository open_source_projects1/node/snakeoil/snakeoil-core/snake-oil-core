import LoggerTransport from "../LoggerTransport.js"

class ConsoleTransport extends LoggerTransport {

    constructor(options) {
        super(options)
    }

    write(msg){
        if(this._options && this._options.destination === "stderr"){
            process.stderr.write(msg)
            return;
        }

        process.stdout.write(msg)
    }


}

export default ConsoleTransport