import fs from "node:fs"
import readline from "node:readline"

let rd = readline.createInterface({
    input: fs.createReadStream('./logs/1.log'),
    output: process.stdout,
    console: false
});
let counter = 1;
rd.on('line', function(line) {
    console.log(`line ${counter}`, JSON.parse(line));
    counter++
});