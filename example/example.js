import SnakeLogger from "../index.js";
import SonicBoomTransport from "snakeoil-logger-sonic-boom-transport";
import chalk from 'chalk';


let colorsMap = {
    "debug": "green",
    "info": "blue",
    "warn": "yellow",
    "error": "red",
}

let logger = new SnakeLogger.SimpleLogger({
    level: "error"
})
logger = SnakeLogger.DefaultPrefill(logger)
let name = "1"
setTimeout(function () {
    name = "2"
}, 1000)
let fdp = new SnakeLogger.FileDescriptorProvider({name:function () {
        return name
    }, "filepath": "./logs", "ending": ".log"})
logger
    .registerTransformer("*", ["SonicBoomTransport"], function colorize(obj, type) {
        if(type === "meta" && obj.level){
            obj.level = chalk[colorsMap[obj.level]](obj.level)
        }
        return obj
    })
    .registerSerializer("ConsoleTransport", new SnakeLogger.LoggerJsonSerializer({pretty: true}))
    .registerSerializer("SonicBoomTransport", new SnakeLogger.LoggerJsonSerializer({pretty: false}))
    .registerTransport("ConsoleTransport", "*", new SnakeLogger.ConsoleTransport({}))
    .registerTransport("SonicBoomTransport", "*", new SonicBoomTransport({
        fd: () => { return fdp.provideSync("a")},
        sync: true
    }))


logger.error(new Error("some error"))
setTimeout(() => {
    logger.error(new Error("some error in new file"))
}, 2000)