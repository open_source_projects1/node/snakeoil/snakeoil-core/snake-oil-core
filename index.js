import SimpleLogger from "./src/SimpleLogger.js";
import FileDescriptorProvider from "./src/FileDescriptorProvider.js";
import DefaultPrefill from "./src/DefaultPrefill.js";
import LoggerJsonSerializer from "./src/serializers/LoggerJsonSerializer.js";
import PassthroughSerializer from "./src/serializers/PassthroughSerializer.js";
import ConsoleTransport from "./src/transports/ConsoleTransport.js";

export default {
    SimpleLogger,
    FileDescriptorProvider,
    DefaultPrefill,
    LoggerJsonSerializer,
    PassthroughSerializer,
    ConsoleTransport
}